-- | Direct importing of this module is not safe. Don't use
-- data constructor directly if you accidentally imported.

module Data.HSet.Internal where
       -- ( -- * Type
       --   HSet(..)
       --   -- * Construction
       -- , empty
       -- , singleton
       -- ) where

import           Control.DeepSeq
import           Data.Dynamic
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import           Data.Tagged
import           Data.Typeable
import           Prelude hiding (lookup)
import           TypeFun.Constraint
import           TypeFun.Data.List

#if !(MIN_VERSION_base(4, 8, 0))
import           Control.Applicative
#endif

data HSet (els :: [*])
     = HSet (Map TypeRep Dynamic)

instance Eq (HSet '[]) where
  _ == _ = True

instance (Typeable a, Eq a, Eq (HSet as)) => Eq (HSet (a ': as)) where
  h1@(HSet m1) == h2@(HSet m2) =
    let e1 :: a
        e1 = lookup h1
        e2 = lookup h2
        r1 :: HSet as
        r1 = HSet m1
        r2 = HSet m2
    in case e1 == e2 of
    True -> r1 == r2
    False -> False

instance Ord (HSet '[]) where
  compare _ _ = EQ

instance (Typeable a, Ord a, Ord (HSet as)) => Ord (HSet (a ': as)) where
  compare h1@(HSet m1) h2@(HSet m2) =
    let e1 :: a
        e1 = lookup h1
        e2 = lookup h2
        r1 :: HSet as
        r1 = HSet m1
        r2 = HSet m2
    in case compare e1 e2 of
    EQ -> compare r1 r2
    ret -> ret

instance Show (HSet '[]) where
  show _ = "empty"

instance (Typeable a, Show a, Show (HSet as))
         => Show (HSet (a ': as)) where
  show h@(HSet m) =
    let e :: a
        e = lookup h
        p = typeRep (Proxy :: Proxy a)
        rest :: HSet as
        rest = HSet m
    in "insert (" ++ show e ++ " :: " ++ show p ++ ") $ " ++ show rest

instance NFData (HSet '[]) where
  rnf _ = ()

instance (Typeable a, NFData a, NFData (HSet as))
         => NFData (HSet (a ': as)) where
  rnf h@(HSet m) =
    let e :: a
        e = lookup h
        rest :: HSet as
        rest = HSet m
    in rnf e `seq` rnf rest

-- $ Internal

mapKeyVal :: forall a. (Typeable a) => a -> (TypeRep, Dynamic)
mapKeyVal a = (key, val)
  where
    p = Proxy :: Proxy a
    key = typeRep p
    val = toDyn a

singleTMap :: Typeable a => a -> Map TypeRep Dynamic
singleTMap a = (uncurry M.singleton) $ mapKeyVal a

class Keys (keys :: [*]) where
  getKeys :: proxy keys -> [TypeRep]

instance Keys '[] where
  getKeys _ = []

instance (Typeable a, Keys as) => Keys (a ': as) where
  getKeys _ = (typeRep (Proxy :: Proxy a)) : getKeys (Proxy :: Proxy as)


-- | Cast one hset to another without any type checking. Narrows
-- internal map to given list of elements.
narrowUnsafe :: forall els els'. (Keys els') => HSet els -> HSet els'
narrowUnsafe (HSet m) =
  let keys = getKeys (Proxy :: Proxy els')
      imap = M.fromList $ zip keys $ repeat ()
  in HSet $ M.intersection m imap

-- | Insert or replace element in internal map not taking into account
-- types. Dont use directly.
insertUnsafe :: (Typeable a) => a -> HSet els -> HSet els'
insertUnsafe a (HSet m) =
  let (k, v) = mapKeyVal a
  in HSet $ M.insert k v m

unionUnsafe :: HSet a -> HSet b -> HSet c
unionUnsafe (HSet a) (HSet b) = HSet $ M.union a b

-- $ Construction

empty :: HSet '[]
empty = HSet M.empty

singleton :: Typeable a => a -> HSet '[a]
singleton = HSet . singleTMap

-- $$ Insertion

reorder :: (Reordered els els') => HSet els -> HSet els'
reorder (HSet m) = HSet m

-- $$ Narrowing

narrow :: (SubList els' els, Keys els')
          => HSet els -> HSet els'
narrow = narrowUnsafe

-- $ Lookup

lookup :: forall els a. (Typeable a, Elem a els) => HSet els -> a
lookup (HSet m) =
  let key = typeRep (Proxy :: Proxy a)
      dyn = case M.lookup key m of
        Nothing -> error $ "HSet: element with type " ++ show key ++ " not found, this is a bug!"
        Just a  -> a
      ret = case fromDynamic dyn of
        Nothing -> error $ "HSet: contains element with wrong type: " ++ show (dynTypeRep dyn) ++ " but should be " ++ show key
        Just a -> a
  in ret

lookupTagged :: forall proxy els t a.
                (Typeable (Tagged t a), Elem (Tagged t a) els)
                => proxy t -> HSet els -> a
lookupTagged _ h =
  let t :: Tagged t a
      t = lookup h
  in unTagged t

-- $ Combine

-- $$ Union

union :: HSet a -> HSet b -> HSet (Union b a)
union = unionUnsafe

append :: (NotSubList a b) => HSet a -> HSet b -> HSet (a :++: b)
append = unionUnsafe

-- $$ Intersection

intersection :: HSet a -> HSet b -> HSet (Intersect a b)
intersection (HSet a) (HSet b) =
  HSet $ M.intersection a b
