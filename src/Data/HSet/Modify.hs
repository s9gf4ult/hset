module Data.HSet.Modify where
       -- ( HModify(..)
       -- , HModifiable
       -- , HMonoModifiable
       -- , hMonoModify
       -- , hModifyTagged
       -- , hMonoModifyTagged
       -- , hUntag
       -- , hTag
       -- ) where

import           Data.HSet.Internal
import qualified Data.Map.Strict as M
import           Data.Tagged
import           Data.Typeable
import           Prelude hiding (lookup)
import           TypeFun.Data.List
import           TypeFun.Data.Peano

#if !(MIN_VERSION_base(4, 8, 0))
import           Control.Applicative
#endif

-- $ Insertion

repsert :: (Typeable a)
           => a -> HSet els -> HSet (AppendUniq a els)
repsert = insertUnsafe

insert :: (Typeable a, NotElem a els)
          => a -> HSet els -> HSet (a ': els)
insert = insertUnsafe

replace :: (Typeable a, Elem a els)
           => a -> HSet els -> HSet els
replace = insertUnsafe

-- $ Delete

delete :: forall proxy els a. (Typeable a, Elem a els)
          => proxy a -> HSet els -> HSet (Delete a els)
delete _ (HSet m) =
  let key = typeRep (Proxy :: Proxy a)
  in HSet $ M.delete key m

-- $ Modify

modify :: (Typeable a, Elem a els)
          => (a -> a) -> HSet els -> HSet els
modify f h =
  let e = f $ lookup h
  in replace e h

hModify :: ( Typeable a, Typeable b
           , Elem a els, NotElem b els )
           => (a -> b) -> HSet els -> HSet (Replace a b els)
hModify f h =
  let e = f $ lookup h
  in insertUnsafe e h

-- $$ Tagged

modifyTagged :: forall proxy t a els.
                ( Typeable a, Typeable (Tagged t a)
                , Elem (Tagged t a) els )
                => proxy t -> (a -> a) -> HSet els -> HSet els
modifyTagged _ f = modify (fmap f :: Tagged t a -> Tagged t a)

hModifyTagged :: forall proxy els t a b.
                 ( Typeable a, Typeable b
                 , Typeable (Tagged t a), Typeable (Tagged t b)
                 , Elem (Tagged t a) els, NotElem (Tagged t b) els )
                 => proxy t -> (a -> b) -> HSet els -> HSet (Replace (Tagged t a) (Tagged t b) els)
hModifyTagged _ f = hModify (fmap f :: Tagged t a -> Tagged t b)

hTag :: forall proxy1 proxy2 t a els.
       ( Typeable a, Typeable (Tagged t a)
       , Elem a els, NotElem (Tagged t a) els )
       => proxy1 t -> proxy2 a -> HSet els -> HSet (Replace a (Tagged t a) els)
hTag _ _ = hModify (Tagged :: a -> Tagged t a)

hUntag :: forall proxy1 proxy2 t a els.
         ( Typeable a, Typeable (Tagged t a)
         , Elem (Tagged t a) els, NotElem a els )
         => proxy1 t -> proxy2 a -> HSet els -> HSet (Replace (Tagged t a) a els)
hUntag _ _ = hModify (unTagged :: Tagged t a -> a)

hRetag :: forall proxy1 proxy2 proxy3 t1 t2 a els.
          ( Typeable a, Typeable (Tagged t1 a), Typeable (Tagged t2 a)
          , Elem (Tagged t1 a) els, NotElem (Tagged t2 a) els )
          => proxy1 t1 -> proxy2 t2 -> proxy3 a -> HSet els -> HSet (Replace (Tagged t1 a) (Tagged t2 a) els)
hRetag _ _ _ = hModify (retag :: Tagged t1 a -> Tagged t2 a)
