module Main where

import           Data.HSet (HSet)
import qualified Data.HSet as H
import           Data.Proxy
import           TypeFun.Data.List

type Els =
  '[ Int, Double, String ]

type SubEls =
  '[ Int, Double ]

intPlusDouble :: (SubList Els els) => HSet els -> Double
intPlusDouble h =
  let i = H.lookup h :: Int
      d = H.lookup h :: Double
  in d + realToFrac i

takeSub :: (SubList SubEls els) => HSet els -> HSet SubEls
takeSub = H.narrow

multiSub :: (SubList Els els, SubList SubEls (Delete String els))
            => HSet els -> ()
multiSub h =
  let d = intPlusDouble h
      rm = H.delete (Proxy :: Proxy String) h
      x = takeSub rm
  in d `seq` x `seq` ()

-- multiSub

main :: IO ()
main = do
  return ()
