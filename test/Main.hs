module Main where

import           Control.Monad
import           Data.HSet (HSet)
import qualified Data.HSet as H
import           Data.Proxy
import           Data.Tagged
import           Test.HUnit

hset :: HSet '[Int, Double, Integer, Float]
hset = H.insert 1
     $ H.insert 2
     $ H.insert 3
     $ H.singleton 4

hsetEq :: Bool
hsetEq = hset == hset

hsetCompare :: Ordering
hsetCompare = compare hset hset

hsetInt :: Int
hsetInt = H.lookup hset

hsetDoube :: Double
hsetDoube = H.lookup hset

hsetInteger :: Integer
hsetInteger = H.lookup hset

hsetFloat :: Float
hsetFloat = H.lookup hset

data Label = This | That

labeledHSet :: HSet '[Tagged 'This Int, Tagged 'That Int, Tagged "hello" String]
labeledHSet = H.insert (Tagged 1)
            $ H.insert (Tagged 2)
            $ H.singleton (Tagged "olleh")


main :: IO ()
main = void $ runTestTT $ TestList
    [ TestList
      [ TestCase $ hsetEq @?= True
      , TestCase $ hsetCompare @?= EQ
      , TestCase $ hsetInt @?= 1
      , TestCase $ hsetDoube @?= 2
      , TestCase $ hsetInteger @?= 3
      , TestCase $ hsetFloat @?= 4
      ]
    , TestList
      [ TestCase $ (H.lookupTagged (Proxy :: Proxy 'This) labeledHSet) @?= (1 :: Int)
      , TestCase $ (H.lookupTagged (Proxy :: Proxy 'That) labeledHSet) @?= (2 :: Int)
      , TestCase $ (H.lookupTagged (Proxy :: Proxy "hello") labeledHSet) @?= "olleh"
      ]
    ]
